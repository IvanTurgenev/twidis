package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"os/signal"
	"syscall"
	// "github.com/davecgh/go-spew/spew"
	"github.com/ecnepsnai/discord"
	"github.com/gempir/go-twitch-irc/v2"
	"github.com/logrusorgru/aurora"
	"github.com/nicklaw5/helix"
	"github.com/robfig/cron"
	"gopkg.in/ini.v1"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strings"
	"time"
	"github.com/davecgh/go-spew/spew"
)

var (
	clientID            string
	clientSecret        string
	externalURL         string
	appAccessToken      string
	currentTwitchClient *helix.Client
	serverPort          string
	// these are used to authenticate requests that require user permissions
	userAccessToken  string
	userRefreshToken string
	channelID        string
	channelName      string
	subscribers      []string
	crono            *cron.Cron
	followsTopic     []string
	subsTopic        []string
	verbose          bool
	tclient          *twitch.Client
)
type subscriptionWebhook struct {
	helix.ResponseCommon
	Data manyEvents
}

type manyEvents struct {
	Events []event `json:"data"`
}
type event struct {
	ID           string             `json:"id"`
	Subscription helix.Subscription `json:"event_data"`
}

func fileExists(filename string) bool {
	info, err := os.Stat(filename)
	if os.IsNotExist(err) {
		return false
	}
	return !info.IsDir()
}

func createConfiguration() {
	if fileExists("config.ini") {

	} else {
		fmt.Println("Creating config file")
		cfg := ini.Empty()
		cfg.Section("").NewKey("clientID", "xxx")
		cfg.Section("").NewKey("clientSecret", "xxx")
		cfg.Section("").NewKey("externalURL", "xxx")
		cfg.Section("").NewKey("serverPort", "xxx")
		cfg.Section("").NewKey("channelName", "xxx")
		cfg.Section("").NewKey("channelID", "xxx")
		cfg.Section("").NewKey("discordWebhookURL", "xxx")
		err1 := cfg.SaveTo("config.ini")
		if err1 != nil {
			fmt.Println(err1)
		}
		fmt.Println("Exiting edit new config file, and start again")
		os.Exit(1)
	}

}

func loadConfiguration() {
	cfgini, err := ini.Load("config.ini")
	if err != nil {
		fmt.Printf("Failed reading config file: %v", err)
		os.Exit(1)
	}
	clientID = cfgini.Section("").Key("clientID").String()
	clientSecret = cfgini.Section("").Key("clientSecret").String()
	externalURL = cfgini.Section("").Key("externalURL").String()
	serverPort = cfgini.Section("").Key("serverPort").String()
	channelName = cfgini.Section("").Key("channelName").String()
	channelID = cfgini.Section("").Key("channelID").String()
	discord.WebhookURL = cfgini.Section("").Key("discordWebhookURL").String()
	followsTopic = []string{
		"https://api.twitch.tv/helix/users/follows?first=1&to_id=" + channelID,
		"/webhooks/twitch/users/follows",
	}
	subsTopic = []string{
		"https://api.twitch.tv/helix/subscriptions/events?first=1&broadcaster_id=" + channelID,
		"/webhooks/twitch/subscriptions/events",
	}

}

func terrorLog(e error, msg string) {
	if e == nil {
		e = errors.New(msg)
	}
	log.Printf("%s: %s", aurora.Red(msg), e)
}

func terrorFatal(e error, msg string) {
	if e == nil {
		e = errors.New(msg)
	}
	// only log to sentry if on production
	log.Fatalf("%s: %s", aurora.Red(msg), e)
}

func subscribeToWebhook(pair []string) {
	topic := pair[0]
	endpoint := pair[1]

	//TODO: eventually use Secret param too
	_, err := currentTwitchClient.PostWebhookSubscription(&helix.WebhookSubscriptionPayload{
		Mode:         "subscribe",
		Topic:        topic,
		Callback:     externalURL + endpoint,
		LeaseSeconds: 24 * 60 * 60, // 24h is the max allowed
	})

	if err != nil {
		terrorLog(err, "failed to create webhook subscription for "+endpoint)
	}
}

func unsubscribeToWebhook() {
	topic := followsTopic[0]
	endpoint := followsTopic[1]
	log.Println("Unsubscribing...")

	//TODO: eventually use Secret param too
	_, err := currentTwitchClient.PostWebhookSubscription(&helix.WebhookSubscriptionPayload{
		Mode:         "unsubscribe",
		Topic:        topic,
		Callback:     externalURL + endpoint,
		LeaseSeconds: 24 * 60 * 60, // 24h is the max allowed
	})

	if err != nil {
		terrorLog(err, "failed to create webhook subscription for "+endpoint)
	}
}

func updateWebhookSubscriptions() {
	subscribeToWebhook(followsTopic)
	subscribeToWebhook(subsTopic)
}

func iniClient() (*helix.Client, error) {
	// use the existing client if we have one
	if currentTwitchClient != nil {
		return currentTwitchClient, nil
	}
	client, err := helix.NewClient(&helix.Options{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		// this is set at https://dev.twitch.tv/console/apps
		RedirectURI: externalURL + "/auth/callback",
		//TODO: move to configs lib
		Scopes: []string{"openid", "user:edit:broadcast", "channel:read:subscriptions"},
	})
	if err != nil {
		terrorLog(err, "error creating client")
	}

	// set the AppAccessToken
	resp, err := client.GetAppAccessToken()
	if err != nil {
		terrorLog(err, "error getting app access token from twitch")
	}
	appAccessToken = resp.Data.AccessToken
	// spew.Dump(appAccessToken)
	client.SetAppAccessToken(appAccessToken)

	// use this as the shared client
	currentTwitchClient = client

	return client, err
}
func generateUserAccessToken(code string) {
	resp, err := currentTwitchClient.GetUserAccessToken(code)
	if err != nil {
		terrorLog(err, "error getting user access token from twitch")
		return
	}

	userAccessToken = resp.Data.AccessToken
	userRefreshToken = resp.Data.RefreshToken

	// update the current client with the access token
	currentTwitchClient.SetUserAccessToken(userAccessToken)



}

func decodeSubscriptionWebhookResponse(r *http.Request) (*subscriptionWebhook, error) {
	log.Println("decoding subscription webhook")

	// we use a custom struct because the 3rd party lib doesnt support webhooks yet
	resp := &subscriptionWebhook{}
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		terrorLog(err, "failed to read request body")
		return resp, err
	}

	// print the webhook contents
	fmt.Println(string(bodyBytes) + "\n")

	// Only attempt to decode the response if we have a response we can handle
	if len(bodyBytes) > 0 && resp.StatusCode < http.StatusInternalServerError {
		if resp.StatusCode < http.StatusBadRequest {
			// if resp.Data != nil && resp.StatusCode < http.StatusBadRequest {
			// Successful request
			err = json.Unmarshal(bodyBytes, &resp.Data)
		} else {
			// Failed request
			err = json.Unmarshal(bodyBytes, &resp)
		}

		if err != nil {
			terrorLog(err, "failed to decode API response")
		}
	}

	spew.Dump(resp.Data)
	return resp, err
}

func decodeFollowWebhookResponse(r *http.Request) (*helix.UsersFollowsResponse, error) {
	log.Println("decoding user webhook")
	// resp := &helix.Response{}

	resp := &helix.UsersFollowsResponse{}
	bodyBytes, err := ioutil.ReadAll(r.Body)
	if err != nil {
		terrorLog(err, "failed to read request body")
		return resp, err
	}

	// print the webhook contents
	// fmt.Println(string(bodyBytes) + "\n")

	// Only attempt to decode the response if we have a response we can handle
	if len(bodyBytes) > 0 && resp.StatusCode < http.StatusInternalServerError {
		if resp.StatusCode < http.StatusBadRequest {
			// if resp.Data != nil && resp.StatusCode < http.StatusBadRequest {
			// Successful request
			err = json.Unmarshal(bodyBytes, &resp.Data)
		} else {
			// Failed request
			err = json.Unmarshal(bodyBytes, &resp)
		}

		if err != nil {
			terrorLog(err, "failed to decode API response")
		}
	}
	return resp, err
}

func handle(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		if strings.HasPrefix(r.URL.Path, "/webhooks/twitch") {
			log.Println("got webhook challenge request at", r.URL.Path)

			challenge, ok := r.URL.Query()["hub.challenge"]
			if !ok || len(challenge[0]) < 1 {
				terrorLog(nil, "something went wrong with the challenge")
				log.Printf("%#v", r.URL.Query())
				http.Error(w, "404 not found", http.StatusNotFound)
				return
			}
			log.Println("returning challenge")
			fmt.Fprintf(w, string(challenge[0]))

			// this endpoint returns private twitch access tokens
		} else if r.URL.Path == "/auth/callback" {
			codes, ok := r.URL.Query()["code"]

			if !ok || len(codes[0]) < 1 {
				msg := "no code in response from twitch"
				terrorLog(errors.New("code missing"), msg)
				//TODO: better error than StatusNotFound (404)
				http.Error(w, msg, http.StatusNotFound)
				return
			}
			code := string(codes[0])

			log.Println(aurora.Cyan("successfully received token from twitch!"))
			// use the code to generate an access token
			generateUserAccessToken(code)

			//TODO: return a pretty HTML page here (black background, logo, etc)
			fmt.Fprintf(w, "Success!")
			return

			// return a favicon if anyone asks for one
		} else {
			http.Error(w, "404 not found", http.StatusNotFound)
			log.Println("someone tried hitting", r.URL.Path)
			return
		}

	case "POST":
		// user webhooks are received via POST at this url
		//TODO: we can use helix.GetWebhookTopicFromRequest() and
		// share a webhooks URL
		if r.URL.Path == "/webhooks/twitch/users/follows" {

			resp, err := decodeFollowWebhookResponse(r)
			if err != nil {
				terrorLog(err, "error decoding follow webhook")
				//TODO: better error
				http.Error(w, "404 not found", http.StatusNotFound)
				return
			}

			for _, follower := range resp.Data.Follows {
				username := follower.FromName
				log.Println("got webhook for new follower:", username)
				// users.LoginIfNecessary(username)
				// announce new follower in chat
				// chatbot.AnnounceNewFollower(username)
				// discord.Say("NEW USER FOLLOW: " + username)
			}

			fmt.Fprintf(w, "OK")

			// these are sent when users subscribe
		} else if r.URL.Path == "/webhooks/twitch/subscriptions/events" {


			resp, err := decodeSubscriptionWebhookResponse(r)
			if err != nil {
				terrorLog(err, "error decoding subscription webhook")
				//TODO: better error
				http.Error(w, "404 not found", http.StatusNotFound)
				return
			}

			for _, event := range resp.Data.Events {
				username := event.Subscription.UserName
				log.Println("got webhook for new sub:", username)
				// users.LoginIfNecessary(username)
				// announce new sub in chat
				// chatbot.AnnounceSubscriber(event.Subscription)
				discord.Say("NEW SUBSCRIPTION: "+ username)
			}

			// update the internal subscribers list
			// mytwitch.GetSubscribers()

			fmt.Fprintf(w, "OK")
		}else {
			// someone tried to make a post and we dont know what to do with it
			http.Error(w, "404 not found", http.StatusNotFound)
			log.Println("someone tried posting to", r.URL.Path)
			return
		}
	// someone tried a PUT or a DELETE or something
	default:
		fmt.Fprintf(w, "Only GET/POST methods are supported.\n")
	}
}

func startServer() {
	var err error
	log.Println("Starting web server on port", serverPort)

	http.HandleFunc("/", handle)
	port := fmt.Sprintf(":%s", serverPort)

	//TODO: replace certs with autocert: https://stackoverflow.com/a/40494806
	// unfortunately autocert assumes the ports are on 80 and 443
	err = http.ListenAndServe(port, nil)

	if err != nil {
		terrorFatal(err, "couldn't start server")
	}
}

func gracefulShutdown() {
	// signChan channel is used to transmit signal notifications.
	signChan := make(chan os.Signal, 1)
	// Catch and relay certain signal(s) to signChan channel.
	signal.Notify(signChan, os.Interrupt, syscall.SIGTERM)
	// Blocking until a signal is sent over signChan channel. Progress to
	// next line after signal
	<-signChan
	// log.Println("Unsubscribing...")
	// unsubscribeToWebhook() NOT WORKING

	log.Println("Shutting down...")
	os.Exit(1)
}

func main() {
	createConfiguration()
	loadConfiguration()
	go gracefulShutdown()
	go startServer()
	_, err := iniClient()
	if err != nil {
		terrorFatal(err, "unable to create twitch API client")
	}
	updateWebhookSubscriptions()
	for {
		time.Sleep(time.Minute)
	}
}
