module gitlab.com/IvanTurgenev/twidis

go 1.14

require (
	github.com/davecgh/go-spew v1.1.1
	github.com/ecnepsnai/discord v1.1.1
	github.com/gempir/go-twitch-irc/v2 v2.4.0
	github.com/logrusorgru/aurora v2.0.3+incompatible
	github.com/nicklaw5/helix v0.5.9
	github.com/robfig/cron v1.2.0
	github.com/smartystreets/goconvey v1.6.4 // indirect
	gopkg.in/ini.v1 v1.57.0
)
