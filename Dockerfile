# builder image
FROM golang:alpine as builder
RUN mkdir /build
WORKDIR /build
COPY go.mod .
COPY go.sum .

RUN go mod download
COPY . .
ENV GO111MODULE=on
RUN CGO_ENABLED=0 GOOS=linux go build -a -o main .


# generate clean, final image for end users
FROM alpine:latest
RUN mkdir /app
WORKDIR /app
COPY --from=builder /build/main .
ADD config.ini .
# executable
ENTRYPOINT [ "./main" ]
